# About
This project was created using the Core UI React Admin template. The purpose of this demo project is to show video conferencing and receiving bluetooth measurements sent via a tablet. The website is updated using Signalr so that the user doesn't need to refresh the page to see the incoming measurements.

# Developing
Run this command to download required packages:
```
yarn
```

This command to start:
```
yarn start
```

This command to build a release version that will be in the build folder
```
yarn build
```