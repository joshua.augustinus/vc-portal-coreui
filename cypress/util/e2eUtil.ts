let counter = 0;
const sizes = ["iphone-6", "ipad-2", [1024, 768]];

export function type(selector: string, text: string) {
  cy.get("[data-cy=" + selector + "]").type(text);
}

export function get(selector: string) {
  return cy.get("[data-cy=" + selector + "]");
}

export function screenshot() {
  cy.screenshot(counter.toString());

  counter++;
}
