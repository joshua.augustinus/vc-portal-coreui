import { Patient, WaitingRoomAction } from "../types";

export function waitingRoomUpdated(patients: Patient[]) {
  return { type: "WAITING_ROOM_UPDATED", patients: patients };
}

export function isBusy(isBusy: boolean) {
  return { type: "BUSY_UPDATED", isBusy: isBusy };
}
