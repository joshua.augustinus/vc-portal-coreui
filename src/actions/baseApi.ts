import { store } from "../store";
import { HttpResponse } from "@microsoft/signalr";
/**
 * https://www.npmjs.com/package/fetch-retry
 */
require("es6-promise").polyfill();
var originalFetch = require("cross-fetch");
var fetch = require("fetch-retry")(originalFetch, {
  retries: 1,
  retryDelay: 1000,
});

export function getAsync(route: string, baseUrl: string) {
  var url = baseUrl + "/" + route;
  var accessToken = getAccessToken();

  if (!accessToken) throw "Couldn't get access token";

  return fetch(url, {
    headers: {
      "content-type": "application/json",
      Authorization: "Bearer " + accessToken,
    },
    mode: "cors",
  })
    .then((response: any) => response.json())
    .catch((error: any) => {
      console.error(error);
      throw error;
    });
}

export function putAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, "PUT", baseUrl);
}

export function postAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, "POST", baseUrl);
}

export function deleteAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, "DELETE", baseUrl);
}

function pAsync(route: string, data: any, method: string, baseUrl: string) {
  let url = baseUrl + "/" + route;
  let accessToken = getAccessToken();
  let status: number;
  let stringData = JSON.stringify(data);

  console.log("posting data", stringData);

  return fetch(url, {
    headers: {
      "content-type": "application/json",
      Authorization: "Bearer " + accessToken,
    },
    method: method,
    body: stringData,
    mode: "cors",
  })
    .then((response: any) => {
      status = response.status;

      //Status 204 will not have the content-length header
      let contentType = response.headers.get("Content-Type");
      if (contentType && contentType.includes("application/json")) {
        console.log("Attempting response.json");
        return response.json();
      } else return {};
    })
    .then((responseJson: any) => {
      let customResponse: HttpResponse = { ...responseJson, status: status };
      return customResponse;
    })
    .catch((error: any) => {
      console.log(error);
      throw error;
    });
}

//Demo project no auth
export function getAccessToken() {
  //need to do something here if accesstoken expired
  //show user a message
  let state = store.getState();
  console.log("state: ", state);
  if (state.login.authResponse) {
    return state.login.authResponse.access_token;
  } else {
    return null;
  }
}
