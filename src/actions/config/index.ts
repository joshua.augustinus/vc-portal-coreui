interface urls {
  signalr: string;
  fcm: string;
  user: string;
}

export const PlatformTypes = {
  LOCALHOST: "localhost",
  AZURE: "azure",
};

//platforms is like Dictionary<String, urls>();
var platforms: { [platformName: string]: urls } = {};
platforms[PlatformTypes.LOCALHOST] = {
  signalr: "http://localhost:9156",
  fcm: "https://localhost:44361",
  user: process.env.REACT_APP_USER_URL,
};

platforms[PlatformTypes.AZURE] = {
  signalr: process.env.REACT_APP_SIGNALR_URL,
  fcm: process.env.REACT_APP_FCM_URL,
  user: process.env.REACT_APP_USER_URL,
};

export default platforms;
