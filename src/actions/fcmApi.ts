import * as baseApi from "./baseApi";
import * as environment from "./environment";
import { HttpResponse } from "@microsoft/signalr";
import { isBusy } from "./actionCreator";

const baseUrl = environment.getEnvironment().fcm;
function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

function putAsync(route: string, data: any) {
  return baseApi.putAsync(route, data, baseUrl);
}

function postAsync(route: string, data: any) {
  return baseApi.postAsync(route, data, baseUrl);
}

function deleteAsync(route: string, data: any) {
  return baseApi.deleteAsync(route, data, baseUrl);
}

export function createSession(
  ticketNumber: number,
  patientName: string,
  registrationToken: string
) {
  return function (dispatch: any) {
    dispatch(isBusy(true));
    let payload = {
      registrationToken: registrationToken,
      ticketNumber: ticketNumber,
    };
    return postAsync("session", payload)
      .then((result: HttpResponse) => {
        dispatch({
          type: "SESSION_UPDATED",
          data: { ...result, recipient: patientName },
        });

        return true;
      })
      .catch((error: any) => {
        alert(error);
        return false;
      })
      .finally(() => {
        dispatch(isBusy(false));
      });
  };
}

export function endSession(registrationToken: string) {
  const payload = { registrationToken: registrationToken };

  return deleteAsync("session", payload)
    .then((result: HttpResponse) => {
      console.log("endsession response", result);
      return true;
    })
    .catch((error: any) => {
      alert(error);
      return false;
    });
}
