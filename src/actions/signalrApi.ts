import * as baseApi from "./baseApi";
import * as environment from "./environment";
import { waitingRoomUpdated } from "./actionCreator";
import { any } from "prop-types";
import { Patient } from "../types";

const baseUrl = environment.getEnvironment().signalr;
function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

function putAsync(route: string, data: any) {
  return baseApi.putAsync(route, data, baseUrl);
}

function postAsync(route: string, data: any) {
  return baseApi.postAsync(route, data, baseUrl);
}

function deleteAsync(route: string, data: any) {
  return baseApi.deleteAsync(route, data, baseUrl);
}

interface GetWaitingRoomResponse {
  patientNames: string[];
  patients: Patient[];
}

export function getWaitingRoom() {
  return function (dispatch: any) {
    dispatch({ type: "BUSY_UPDATED", isBusy: true });

    return getAsync("waiting")
      .then((result: GetWaitingRoomResponse) => {
        console.log("GetWaitingRoomResponse", result);
        dispatch(waitingRoomUpdated(result.patients));
        dispatch({ type: "BUSY_UPDATED", isBusy: false });
      })
      .catch((error: any) => {
        dispatch({ type: "BUSY_UPDATED", isBusy: false });
        alert(error);
      });
  };
}

interface LeaveWaitingRoomResponse {
  action: any;
  waitingRoom: GetWaitingRoomResponse;
  status: number;
}

export function leaveWaitingRoom(ticketNumber: number) {
  if (ticketNumber === null) {
    alert("ticketNumber cannot be null");
    throw "ticketNumber cannot be null";
  }
  return deleteAsync("waiting", { ticketNumber: ticketNumber })
    .then((result: LeaveWaitingRoomResponse) => {
      console.log("delete result", result);
    })
    .catch((error: any) => {
      alert(error);
    });
}

export function notifyEndCall(ticketNumber: string) {
  if (ticketNumber === null) {
    alert("ticketNumber cannot be null");
    throw "ticketNumber cannot be null";
  }

  return postAsync("ticketnumber/" + ticketNumber + "/endcall", {});
}

export function joinWaitingRoom(patientName: string) {
  return postAsync("waiting", { patientName: patientName });
}
