import * as baseApi from "./baseApi";
import * as environment from "./environment";
import { Dispatch } from "react";
import { HttpResponse } from "@microsoft/signalr";
import { isBusy } from "./actionCreator";

const baseUrl = environment.getEnvironment().user;

function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

function putAsync(route: string, data: any) {
  return baseApi.putAsync(route, data, baseUrl);
}

function postAsync(route: string, data: any) {
  return baseApi.postAsync(route, data, baseUrl);
}

export function getToken(username: string, password: string) {
  return function (dispatch: any) {
    dispatch(isBusy(true));
    let payload = { username: username, password: password };
    return postAsync("token", payload)
      .then((result: HttpResponse) => {
        dispatch({
          type: "AUTHRESPONSE_UPDATED",
          data: result,
          username: username,
        });
      })
      .catch((error: any) => {
        dispatch({ type: "AUTHRESPONSE_UPDATED", data: error, username: null });
      })
      .finally(() => {
        dispatch(isBusy(false));
      });
  };
}
