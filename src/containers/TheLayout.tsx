import React, { useEffect } from "react";
import { TheContent, TheSidebar, TheFooter, TheHeader } from "./index";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { RootState } from "../types";

const TheLayout = () => {
  const authResponse = useSelector(
    (state: RootState) => state.login.authResponse
  );
  let history = useHistory();

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    // Update the document title using the browser API

    if (!authResponse || !authResponse.access_token) {
      console.log("no auth response");
      //adding random parameter forces page to not use cache
      let randomQuery = (Math.random() * 10000).toString().substring(0, 4);

      history.replace("/login?r=" + randomQuery);
    }
  }, []);

  if (authResponse === null) {
    return null;
  }

  return (
    <div className="c-app c-default-layout">
      <TheSidebar />
      <div className="c-wrapper">
        <TheHeader />
        <div className="c-body">
          <TheContent />
        </div>
        <TheFooter />
      </div>
    </div>
  );
};

export default TheLayout;
