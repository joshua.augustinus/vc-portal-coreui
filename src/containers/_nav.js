export default [
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: "cil-speedometer",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Waiting Room",
    to: "/waitingroom",
    icon: "cil-av-timer",
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "E2E Testing",
    to: "/e2etesting",
    icon: "cil-braille",
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Detox",
        to: "/e2etesting/Detox",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Cypress",
        to: "/e2etesting/Cypress",
      },
    ],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Postman Testing",
    to: "/postman",
    icon: "cil-envelope-closed",
  },
];
