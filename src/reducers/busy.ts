import { AnyAction } from "redux";
import { BusyReducer } from "../types";

const initialState: BusyReducer = {
  isBusy: false,
  idClicked: null,
};

const changeState = (
  state = initialState,
  { type, ...rest }: AnyAction
): BusyReducer => {
  switch (type) {
    case "BUSY_UPDATED":
      return { ...state, ...rest };
    case "BUTTON_CLICKED":
      return { ...state, ...rest };
    default:
      return state;
  }
};

export default changeState;
