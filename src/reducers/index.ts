import { combineReducers, AnyAction } from "redux";
import coreuiReducer from "./coreui";
import loginReducer from "./login";
import videoReducer from "./videoConference";
import busyReducer from "./busy";
import waitingRoomReducer from "./waitingRoom";
import { RootState } from "../types";

const reducers = combineReducers<RootState>({
  coreui: coreuiReducer,
  login: loginReducer,
  videoConference: videoReducer,
  busy: busyReducer,
  waitingRoom: waitingRoomReducer,
});

const rootReducer = (state: RootState, action: AnyAction) => {
  let fixedState: RootState | undefined;
  if (action.type === "USER_LOGOUT") {
    //Passing this down to reducers will force them to use
    fixedState = undefined;
  } else {
    fixedState = state;
  }
  return reducers(fixedState, action);
};

export default rootReducer;
