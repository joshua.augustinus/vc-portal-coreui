import { AnyAction } from "redux";
import { LoginReducer } from "../types";

const initialState: LoginReducer = {
  authResponse: null,
  username: null,
};

function loginReducer(state = initialState, action: AnyAction): LoginReducer {
  switch (action.type) {
    case "AUTHRESPONSE_UPDATED":
      return { ...state, authResponse: action.data, username: action.username };
    default:
      return state;
  }
}

export default loginReducer;
