import { AnyAction } from "redux";
import { VideoConferenceReducer } from "../types";

const initialState: VideoConferenceReducer = {};

const videoReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case "SESSION_UPDATED":
      console.log(action);
      return { ...state, ...action.data };
    case "SESSION_ENDED":
      return initialState;

    default:
      return state;
  }
};

export default videoReducer;
