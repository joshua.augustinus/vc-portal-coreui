import { WaitingRoomReducer, Patient, WaitingRoomAction } from "../types";

const initialState: WaitingRoomReducer = {
  patients: [],
  simulatorSetting: "medium",

  roomSize: "medium",
};

const waitingRoomReducer = (
  state = initialState,
  action: WaitingRoomAction
): WaitingRoomReducer => {
  console.log("waitingRoomReducer", action);
  switch (action.type) {
    case "WAITING_ROOM_UPDATED":
      return { ...state, patients: action.patients };
    case "SIMULATOR_SETTING_UPDATED":
      return { ...state, simulatorSetting: action.simulatorSetting };

    case "ROOM_SIZE_UPDATED":
      return { ...state, roomSize: action.roomSize };
    default:
      return state;
  }
};

export default waitingRoomReducer;
