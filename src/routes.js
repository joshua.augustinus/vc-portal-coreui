import React from "react";

const Dashboard = React.lazy(() =>
  import("./views/coreui/dashboard/Dashboard")
);
const VideoConference = React.lazy(() =>
  import("./views/pages/videoconference")
);
const WaitingRoom = React.lazy(() => import("./views/pages/waitingRoom"));
const Detox = React.lazy(() => import("./views/pages/e2e/detox"));
const Cypress = React.lazy(() => import("./views/pages/e2e/cypress"));
const Postman = React.lazy(() => import("./views/pages/postman"));
const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/videoconference/:ticketNumber/:registrationToken",
    name: "Video Conference",
    component: VideoConference,
  },
  {
    path: "/waitingroom",
    name: "Waiting Room",
    component: WaitingRoom,
  },
  {
    path: "/e2etesting/detox",
    name: "E2E Testing with Detox",
    component: Detox,
  },
  {
    path: "/e2etesting/cypress",
    name: "E2E Testing with Cypress",
    component: Cypress,
  },
  {
    path: "/postman",
    name: "Postman Testing",
    component: Postman,
  },
];

export default routes;
