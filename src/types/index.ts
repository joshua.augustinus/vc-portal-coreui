import { Session } from "@opentok/client";

/**
 * Required for style sheets
 * */
export interface ReactStyleSheet {
  [key: string]: React.CSSProperties;
}

/**
 * Reducer States
 */
export interface RootState {
  coreui: CoreuiReducer;
  login: LoginReducer;
  videoConference: VideoConferenceReducer;
  busy: BusyReducer;
  waitingRoom: WaitingRoomReducer;
}

export interface LoginReducer {
  authResponse: AuthResponse;
  username: string;
}

export interface WaitingRoomReducer {
  patients: Patient[];
  simulatorSetting: string;

  roomSize: RoomSizeType;
}

export interface AuthResponse {
  access_token: string | undefined;
  status: number;
}

export interface CoreuiReducer {}
export interface VideoConferenceReducer {
  token?: string;
  apiKey?: number;
  recipient?: string;
  sessionId?: string;
}
export interface BusyReducer {
  isBusy: boolean;
  idClicked: string;
}

export interface Patient {
  name: string;
  medicareNumber: string;
  ticketNumber: number;
  requestedDoctor: string;
  registrationToken: string;
}

export interface SimSetting {
  lowerBound: number;
  upperBound: number;
  delay: number | null;
}

export interface WaitingRoomAction {
  type: string;
  patients?: Patient[];
  simulatorSetting?: string;

  roomSize?: RoomSizeType;
}

export type RoomSizeType = "small" | "medium" | "large";

export interface BloodPressureRecord {
  type: string;
  systolic: MeasurementValue;
  diastolic: MeasurementValue;
  pulseRate: MeasurementValue;
}

export interface MeasurementValue {
  unit: string;
  value: number;
}
