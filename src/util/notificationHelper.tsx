import { toast } from "react-toastify";
import React from "react";

interface Props {
  color: string;
}

const SimpleToast = (props: any) => {
  return <div style={{ marginLeft: 20 }}>{props.children}</div>;
};

const toastOptions = {
  position: toast.POSITION.BOTTOM_RIGHT,
  hideProgressBar: true,
  autoClose: 5000,
  pauseOnFocusLoss: false,
};

export function showNotificationInfo(message: String) {
  toast.info(<SimpleToast>{message}</SimpleToast>, toastOptions);
}

export function showNotificationWarning(message: String) {
  let toastOptions = {
    position: toast.POSITION.BOTTOM_RIGHT,
    hideProgressBar: true,
    autoClose: 5000,
    pauseOnFocusLoss: false,
  };

  toast.warn(
    <SimpleToast>
      <span className="text-dark">{message}</span>
    </SimpleToast>,
    toastOptions
  );
}
