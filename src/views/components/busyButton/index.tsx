import React, { useState } from "react";
import { CButton } from "@coreui/react";
import { useDispatch, useSelector } from "react-redux";
import uniqid from "uniqid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { RootState } from "types";

const BusyButton = (props: any) => {
  const isBusy = useSelector((state: RootState) => state.busy.isBusy);
  const idClicked = useSelector((state: RootState) => state.busy.idClicked);
  const dispatch = useDispatch();
  const [id, setId] = useState(uniqid());
  //consume props
  const { onClick, ...rest } = props;

  const clickHandler = () => {
    dispatch({ type: "BUTTON_CLICKED", idClicked: id });
    if (onClick) onClick();
  };

  const showSpinner = isBusy && idClicked == id;
  const cursor = props.disabled ? "not-allowed" : "";
  return (
    <CButton
      style={{ cursor: cursor }}
      {...rest}
      disabled={isBusy || props.disabled}
      onClick={clickHandler}
    >
      {showSpinner && (
        <FontAwesomeIcon spin icon={faSpinner} className="mr-2" />
      )}
      {props.children}
    </CButton>
  );
};

export default BusyButton;
