import React from "react";
import { CCard, CCardHeader } from "@coreui/react";

const Card = (props: any) => {
  return <CCard style={{ borderRadius: 10 }}>{props.children}</CCard>;
};

export { Card };

const CardHeader = (props: any) => {
  return (
    <CCardHeader style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
      {props.children}
    </CCardHeader>
  );
};

export { CardHeader };
