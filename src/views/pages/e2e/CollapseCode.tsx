import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardHeader,
  CCardBody,
  CCollapse,
} from "@coreui/react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { darcula } from "react-syntax-highlighter/dist/esm/styles/prism";

interface Props {
  title: string;
  children: any;
}

const CollapseCode = (props: Props) => {
  const [accordion, setAccordion] = useState(null);
  useEffect(() => {});

  return (
    <>
      <CCard className="mb-1">
        <CCardHeader id="headingOne">
          <CButton
            block
            color="link"
            className="text-left m-0 p-0"
            onClick={() => setAccordion(accordion === 0 ? null : 0)}
          >
            <h5 className="m-0 p-0">{props.title}</h5>
          </CButton>
        </CCardHeader>
        <CCollapse show={accordion === 0}>
          <CCardBody>
            <SyntaxHighlighter language="javascript" style={darcula}>
              {props.children}{" "}
            </SyntaxHighlighter>
          </CCardBody>
        </CCollapse>
      </CCard>
    </>
  );
};

export default CollapseCode;
