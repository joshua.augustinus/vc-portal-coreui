import React, { useState, useEffect } from "react";
import { CCol, CRow, CCardBody } from "@coreui/react";
import path from "path";
import styles from "./styles";
import { Card, CardHeader } from "../../components/card";

function getImageNames(indexMin: number, indexMax: number) {
  let imageNames: string[] = [];
  for (let i = indexMin; i <= indexMax; i++) {
    imageNames.push(i.toString() + ".png");
  }

  return imageNames;
}

interface Props {
  title: string;
  /**
   * The name of the folder relative to public folder
   */
  publicFolder: string;
  /**
   * Assumes pictures are of the form 0.png, 1.png, etc.
   */
  indexMin: number;
  indexMax: number;
}

const CypressScreenshots = (props: Props) => {
  const [imgNames, setImgNames] = useState(
    getImageNames(props.indexMin, props.indexMax)
  );
  useEffect(() => {});

  const images = imgNames.map((value, index) => {
    let src = path.join(props.publicFolder, value);
    return (
      <a href={src} target="_blank">
        <img src={src} style={styles.screenshotImg} />
      </a>
    );
  });

  return (
    <>
      <Card>
        <CardHeader>
          <CRow>
            <CCol>
              <h2>{props.title}</h2>
            </CCol>
          </CRow>
        </CardHeader>
        <CCardBody style={styles.flexContainer}>{images}</CCardBody>
      </Card>
    </>
  );
};

export default CypressScreenshots;
