const codeString = `
const tests = (size: any) => {
    before(() => {
      if (Cypress._.isArray(size)) {
        cy.viewport(size[0], size[1]);
      } else {
        cy.viewport(size);
      }
    });
  
    beforeEach(() => {
      cy.visit("http://localhost:3000/#/login");
    });
  
    it("should Login", () => {
      screenshot();
      get("username-input").type("demo!");
      get("password-input").type("demo!");
      get("login-button").click();
      get("dashboard-view");
      cy.wait(2000);
      screenshot();
    });
  

  };
  
`;

export default codeString;
