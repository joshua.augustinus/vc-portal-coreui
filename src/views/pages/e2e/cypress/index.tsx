import React from "react";
import { CCol, CRow, CCardBody } from "@coreui/react";
import { Card, CardHeader } from "../../../components/card";
import Screenshots from "../Screenshots";
import CollapseCode from "../CollapseCode";
import codeString from "./cypressCodeString";

const E2E = (props: any) => {
  return (
    <>
      <Card>
        <CardHeader>
          <CRow>
            <CCol md="4">E2E Testing with Cypress</CCol>
            <CCol
              md="8"
              className="d-flex justify-content-end align-items-center"
            ></CCol>
          </CRow>
        </CardHeader>
        <CCardBody>
          <CollapseCode title="E2E Tests with Cypress (Sample Code)">
            {codeString}
          </CollapseCode>
        </CCardBody>
      </Card>

      <Screenshots
        indexMin={0}
        indexMax={1}
        title="Desktop screenshots"
        publicFolder="images\\cypress\\screenshots\\desktop.ts"
      />

      <Screenshots
        indexMin={0}
        indexMax={1}
        title="Ipad 2 screenshots"
        publicFolder="images\\cypress\\screenshots\\ipad-2.ts"
      />

      <Screenshots
        indexMin={0}
        indexMax={1}
        title="Iphone 6 screenshots"
        publicFolder="images\\cypress\\screenshots\\iphone-6.ts"
      />
    </>
  );
};

export default E2E;
