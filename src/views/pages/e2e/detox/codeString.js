const codeString = `
describe('My app', () => {
    beforeAll(() => {
      if (fs.existsSync(SAVE_DIR)) {
        rimraf.sync(SAVE_DIR);
      }
  
      fs.mkdirSync(SAVE_DIR);
    });
  
    beforeEach(async () => {
      await device.reloadReactNative();
      await expect(testIDs.TRUE_HOME_BUTTON);
    });
  
    it('should go to Login after reset', async () => {
      await tap(testIDs.E2E_RESET_STATE_BUTTON);
      await tap(testIDs.TRUE_HOME_BUTTON);
      await expect(testIDs.LOGIN_TITLE);
      await replaceText(testIDs.USERNAME_INPUT, 'demo!');
      await replaceText(testIDs.PASSWORD_INPUT, 'demo!');
      await tap(testIDs.LOGIN_BUTTON);
      await tap(testIDs.RESET_STATE_BUTTON);
      await expect(testIDs.LOGIN_TITLE);
      await screenshot();
    });
  
    it('should clear ticket number on reset', async () => {
      await tap(testIDs.E2E_RESET_STATE_BUTTON);
      await expect(testIDs.TICKET_NUMBER_FIELD);
      await expect(testIDs.INVALID_TICKET_WARNING);
    });
  
    it('should have Create User screen', async () => {
      await tap(testIDs.TRUE_HOME_BUTTON);
      await expect(testIDs.CREATE_ACCOUNT_BUTTON);
      await tap(testIDs.CREATE_ACCOUNT_BUTTON);
      await expect(testIDs.CREATE_USER_FORM);
      await screenshot();
    });
  
    it('should navigate to Waiting Room', async () => {
      await tap(testIDs.TRUE_HOME_BUTTON);
      await expect(testIDs.USERNAME_INPUT);
      await replaceText(testIDs.USERNAME_INPUT, 'demo!');
      await replaceText(testIDs.PASSWORD_INPUT, 'demo!');
      await tap(testIDs.LOGIN_BUTTON);
      await screenshot();
      await tap(testIDs.JOIN_WAITING_BUTTON);
    });
  
    it('should have pictures in the Waiting Room', async () => {
      for (let i = 0; i <= 10; i++) {
        await replaceText(testIDs.Q_POSITION_INPUT, i.toString());
        await tap(testIDs.E2E_WAITING_BUTTON);
        await screenshot();
        // await device.sendToHome(); //didn't work
        await device.pressBack();
      }
    });
  
    it('should navigate to Waiting Room', async () => {
      await tap(testIDs.TRUE_HOME_BUTTON);
      await tap(testIDs.JOIN_WAITING_BUTTON);
    });
  
    it('should have a video call screen', async () => {
      await tap(testIDs.CREATE_SESSION_BUTTON);
      let timeout = 20000;
      await expect(testIDs.ACCEPT_CALL_BUTTON, timeout);
      await screenshot('Call Incoming');
      await tap(testIDs.ACCEPT_CALL_BUTTON);
      await screenshot();
    });
  });
  
`;

export default codeString;
