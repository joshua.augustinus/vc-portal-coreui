const codeString = `
async function tap(testID) {
    return element(by.id(testID)).tap();
  }
  
  async function expect(testID, timeout = 2000) {
    return waitFor(element(by.id(testID)))
      .toBeVisible()
      .withTimeout(timeout);
  }
  
  async function haveText(testID, expectedText) {
    await expect(element(by.id(testID))).toHaveText(expectedText);
  }
  
  async function replaceText(testID, newText) {
    await element(by.id(testID)).replaceText(newText);
  }
  
  async function screenshot() {
    let name = counter.toString();
    counter++;
    const imagePath = await device.takeScreenshot(name);
    const newPath = path.join(SAVE_DIR, name + '.png');
    console.log('newpath', newPath);
  
    fs.copyFileSync(imagePath, newPath);
  }
`;

export default codeString;
