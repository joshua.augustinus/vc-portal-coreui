import React from "react";
import { CCol, CRow, CCardBody } from "@coreui/react";
import codeString from "./codeString";
import helperCode from "./helperCode";
import Screenshots from "../Screenshots";
import CollapseCode from "../CollapseCode";
import { Card, CardHeader } from "../../../components/card";

const E2E = (props: any) => {
  return (
    <>
      <Card>
        <CardHeader>
          <CRow>
            <CCol md="4">E2E Testing with Detox</CCol>
            <CCol
              md="8"
              className="d-flex justify-content-end align-items-center"
            ></CCol>
          </CRow>
        </CardHeader>
        <CCardBody>
          <CollapseCode title="E2E Tests with Detox (Sample Code)">
            {codeString}
          </CollapseCode>
          <CollapseCode title="Helper Functions">{helperCode}</CollapseCode>
        </CCardBody>
      </Card>
      <Card>
        <CardHeader>
          <CRow>
            <CCol md="4">Detox running on Emulator</CCol>
            <CCol
              md="8"
              className="d-flex justify-content-end align-items-center"
            ></CCol>
          </CRow>
        </CardHeader>
        <CCardBody>
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/awPIiuk6qoo"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </CCardBody>
      </Card>
      <Screenshots
        indexMin={0}
        indexMax={15}
        title="Pixel 2 Phone (5-inch 1080x1920)"
        publicFolder="images\\detox\\Pixel2"
      />
      <Screenshots
        indexMin={0}
        indexMax={15}
        title="Nexus 9 Tablet (8.86-inch 2048x1536)"
        publicFolder="images\\detox\\Nexus9"
      />
    </>
  );
};

export default E2E;
