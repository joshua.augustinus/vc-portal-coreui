import { ReactStyleSheet } from "../../../types";

const style: ReactStyleSheet = {
  flexContainer: {
    flexWrap: "wrap",
    display: "flex",
  },
  screenshotImg: {
    marginRight: 5,
    marginBottom: 5,
    width: 250,
  },
};

export default style;
