import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import * as userApi from "../../../actions/userApi";
import BusyButton from "../../components/busyButton";
import { RootState } from "../../../types";

const GITLAB_URL = "https://gitlab.com/joshua.augustinus";
const RN_PROJECT_URL =
  "https://gitlab.com/joshua.augustinus/react-native-fcm-test";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const dispatch = useDispatch();
  const authResponse = useSelector(
    (state: RootState) => state.login.authResponse
  );

  const history = useHistory();

  const loginPressed = () => {
    //replace this code with auth check
    dispatch(userApi.getToken(username, password));
  };

  const onFormSubmit = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    loginPressed();
  };

  useEffect(() => {
    console.log("AuthResponse", authResponse);
    if (authResponse == undefined) {
      //checks for null or undefined
      return;
    } else if (authResponse.access_token) {
      history.replace("/");
    } else if (authResponse.status === 401) {
      setErrorMessage("Invalid login");
    }
  }, [authResponse]);

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={onFormSubmit}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        data-cy="username-input"
                        type="text"
                        placeholder="Username"
                        autoComplete="username"
                        value={username}
                        onChange={(e: any) => setUsername(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-1">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        data-cy="password-input"
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        value={password}
                        onChange={(e: any) => setPassword(e.target.value)}
                      />
                    </CInputGroup>

                    <span className="text-danger">{errorMessage}</span>

                    <CRow className="mt-4">
                      <CCol xs="6">
                        <BusyButton
                          data-cy="login-button"
                          color="primary"
                          className="px-4"
                          type="submit"
                        >
                          Login
                        </BusyButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard
                className="text-white bg-primary py-5 d-md-down-none"
                style={{ width: "44%" }}
              >
                <CCardBody className="text-center">
                  <div>
                    <h2>Demo Project</h2>
                    <p>
                      This is a demo project for demonstrating video
                      conferencing and bluetooth. It works in conjuction with
                      the react native app from{" "}
                      <a
                        target="_blank"
                        style={{ color: "grey" }}
                        href={RN_PROJECT_URL}
                      >
                        here
                      </a>
                      . (Android support only)
                    </p>

                    <CButton
                      color="light"
                      className="mt-3"
                      active
                      tabIndex={-1}
                      onClick={() => {
                        window.open(GITLAB_URL, "_blank");
                      }}
                    >
                      View all gitlab projects
                    </CButton>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
