import React from "react";
import { CCol, CRow, CCardBody } from "@coreui/react";
import { Card, CardHeader } from "../../components/card";

const Postman = (props: any) => {
  return (
    <>
      <Card>
        <CardHeader>
          <CRow>
            <CCol md="4">API Testing with Postman</CCol>
            <CCol
              md="8"
              className="d-flex justify-content-end align-items-center"
            ></CCol>
          </CRow>
        </CardHeader>
        <CCardBody>
          <p>
            All API endpoints have one or more corresponding Postman tests. In a
            corporate environment these Postman tests would be accessible by all
            in the software team.
          </p>
          <p>Postman tests are a form of contract-based testing.</p>

          <img style={{ width: "100%" }} src="images/postman.png" />
        </CCardBody>
      </Card>
    </>
  );
};

export default Postman;
