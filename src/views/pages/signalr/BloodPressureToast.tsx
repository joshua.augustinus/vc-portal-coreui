import { BloodPressureRecord } from "types";
import React from "react";
import styles from "./styles";
import { MeasurementHeader } from "./MeasurementHeader";

interface MeasurementRowProps {
  type: string;
  valueUnit: string;
}
const MeasurementRow = (props: MeasurementRowProps) => {
  return (
    <div>
      <span style={styles.measurementType}>{props.type}</span> {props.valueUnit}
    </div>
  );
};

const BloodPressureToast = (props: BloodPressureRecord) => {
  const bpValue =
    props.systolic.value +
    "/" +
    props.diastolic.value +
    " " +
    props.systolic.unit;

  const pulseRateValue = props.pulseRate.value + " " + props.pulseRate.unit;

  return (
    <div style={styles.toastContainer}>
      <MeasurementHeader />
      <div style={styles.measurementContainer}>
        <MeasurementRow type="Blood Pressure:" valueUnit={bpValue} />
        <MeasurementRow type="Pulse Rate:" valueUnit={pulseRateValue} />
      </div>
    </div>
  );
};

export { BloodPressureToast };
