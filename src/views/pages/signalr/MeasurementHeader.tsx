import React from "react";
import styles from "./styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faNotesMedical } from "@fortawesome/free-solid-svg-icons";

const MeasurementHeader = () => {
  return (
    <div style={styles.measurementHeader}>
      <FontAwesomeIcon icon={faNotesMedical} />
    </div>
  );
};

export { MeasurementHeader };
