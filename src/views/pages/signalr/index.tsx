import React, { useState, useEffect, useRef } from "react";
import * as signalR from "@microsoft/signalr";
import * as environment from "../../../actions/environment";
import { toast } from "react-toastify";
import styles from "./styles";
import { useDispatch } from "react-redux";
import { waitingRoomUpdated } from "../../../actions/actionCreator";
import {
  WaitingRoomReducer,
  Patient,
  BloodPressureRecord,
} from "../../../types";
import { useHistory } from "react-router-dom";
import {
  showNotificationWarning,
  showNotificationInfo,
} from "../../../util/notificationHelper";
import { BloodPressureToast } from "./BloodPressureToast";
import { MeasurementHeader } from "./MeasurementHeader";

const url = environment.getEnvironment().signalr + "/hub";

interface Props {}

interface MeasurementPayload {
  type: string;
  value: number;
  unit: string;
}

interface WaitingPayload {
  waitingRoom: WaitingRoomReducer;
}

const MeasurementToast = (props: MeasurementPayload) => {
  const measurementTypeText = props.type + ":";
  const measurementText = props.value.toFixed(1) + " " + props.unit;
  return (
    <div style={styles.toastContainer}>
      <MeasurementHeader />
      <div>
        <span style={styles.measurementType}>{measurementTypeText}</span>{" "}
        {measurementText}
      </div>
    </div>
  );
};

const SignalrProvider = (props: Props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  let hubConnection: signalR.HubConnection = null;
  let toastOptions = {
    position: toast.POSITION.BOTTOM_RIGHT,
    hideProgressBar: true,
    autoClose: 5000,
    pauseOnFocusLoss: false,
  };

  let measurementToastOptions: any = { ...toastOptions, autoClose: false };

  /**
   * Function to start hub and retry if failed
   */
  const startConnection = async () => {
    try {
      await hubConnection.start();
      // Once started, invokes the sendConnectionId in our ChatHub inside our ASP.NET Core application.
      if (hubConnection.connectionId) {
        console.log("Hub connected");

        showNotificationInfo("Hub connected");
      }
    } catch (err) {
      console.log(err);
      //Retry
      setTimeout(() => startConnection(), 5000);
    }
  };

  /**
   * Startup wil build hub connection and add listener
   */
  useEffect(() => {
    // Builds the SignalR connection, mapping it to /chat
    hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(url)
      .configureLogging(signalR.LogLevel.Debug)
      .build();

    //Add listener
    hubConnection.on("Measurement", (payload: MeasurementPayload) => {
      console.log("Measurement", payload);
      toast.info(
        <MeasurementToast {...payload}></MeasurementToast>,
        measurementToastOptions
      );
    });

    hubConnection.on("BloodPressure", (payload: BloodPressureRecord) => {
      console.log("BloodPressure", payload);
      toast.info(
        <BloodPressureToast {...payload}></BloodPressureToast>,
        measurementToastOptions
      );
    });

    hubConnection.on("Waiting", (payload: WaitingPayload) => {
      console.log("Waiting", payload);
      dispatch(waitingRoomUpdated(payload.waitingRoom.patients));
    });

    hubConnection.on("IgnoreCall", (payload: Patient) => {
      console.log("IgnoreCall", payload);
      showNotificationWarning("Patient declined the call");
      history.replace("/waitingroom");
    });

    // Starts the SignalR connection
    startConnection();

    return () => {
      console.log("Signalr unmount");
      hubConnection.off("Measurement");
      hubConnection.stop();
    };
  }, []);
  return <></>;
};

export default SignalrProvider;
