import { ReactStyleSheet } from "../../../types";

const styles: ReactStyleSheet = {
  toastContainer: {
    marginLeft: 20,
  },
  measurementHeader: {
    fontSize: 20,
    textAlign: "center",
  },
  measurementType: {
    fontWeight: "bold",
    textAlign: "right",
  },
  measurementContainer: {
    display: "flex",
    flexDirection: "column",
  },
};

export default styles;
