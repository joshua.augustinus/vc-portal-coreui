import React, { useState, useEffect, useRef } from "react";
import styles from "./styles";
import { CCard } from "@coreui/react";
import OT from "@opentok/client";
import { useDispatch } from "react-redux";
import { VideoConferenceReducer } from "../../../types";
import * as signalrApi from "../../../actions/signalrApi";
import * as fcmApi from "../../../actions/fcmApi";
import { useHistory, useParams } from "react-router-dom";
import CallEnd from "@material-ui/icons/CallEnd";
import IconButton from "@material-ui/core/IconButton";
import VideoCallText from "./VideoCallText";

interface RouteParams {
  ticketNumber: string;
  registrationToken: string;
}

const OpenTokContainer = (props: VideoConferenceReducer) => {
  const dispatch = useDispatch();
  const sessionRef = useRef(null);
  const history = useHistory();
  const { ticketNumber, registrationToken } = useParams<RouteParams>();
  const [isAnswered, setIsAnswered] = useState(false);
  const recipient = props.recipient;

  const handleError = (error: OT.OTError) => {
    if (error) {
      alert(error.message);
    }
  };

  const endCallHandler = () => {
    history.replace("/waitingroom");
    console.log("registrationToken", registrationToken);
    if (isAnswered) signalrApi.notifyEndCall(ticketNumber);
    else fcmApi.endSession(decodeURIComponent(registrationToken));
  };

  //https://tokbox.com/developer/tutorials/web/basic-video-chat/
  const initSession = () => {
    let apiKey = props.apiKey;
    let sessionId = props.sessionId;
    let token = props.token;

    sessionRef.current = OT.initSession(apiKey.toString(), sessionId);
    console.log("Inside init session");

    //Create publisher
    let publisher = OT.initPublisher(
      "publisher",
      {
        insertMode: "append",
        width: "100%",
        height: "100%",
      },
      handleError
    );

    //Subscribe to stream
    sessionRef.current.on("streamCreated", (event: any) => {
      sessionRef.current.subscribe(
        event.stream,
        "subscriber",
        {
          insertMode: "append",
          width: "100%",
          height: "100%",
        },
        handleError
      );

      //Make patient leave waiting room
      signalrApi.leaveWaitingRoom(parseInt(ticketNumber));

      setIsAnswered(true);
    });

    //Connect to session
    sessionRef.current.connect(token, (error: any) => {
      // If the connection is successful, publish to the session
      if (error) {
        console.log("error in open tok container");
        handleError(error);
      } else {
        console.log("will publish");
        sessionRef.current.publish(publisher, handleError);
      }
    });
  };

  useEffect(() => {
    initSession();

    return () => {
      console.log("OpenTokContainer dismounted");
      sessionRef.current.disconnect();
      sessionRef.current.off(); //https://tokbox.com/developer/sdks/js/reference/Session.html#off
      dispatch({ type: "SESSION_ENDED" });
    };
  }, []);
  return (
    <CCard>
      <div style={styles.contentContainer}>
        <div id="publisher" style={styles.publisher} />
        <div id="subscriber" style={styles.subscriber} className="bg-success" />
      </div>
      <IconButton
        onClick={endCallHandler}
        className="bg-danger"
        style={styles.endCallButton}
        component="span"
      >
        <CallEnd style={{ fontSize: 40 }} />
      </IconButton>
      <VideoCallText isAnswered={isAnswered} recipient={recipient} />
    </CCard>
  );
};

export default OpenTokContainer;
