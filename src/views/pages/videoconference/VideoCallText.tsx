import React from "react";
import styles from "./styles";

interface Props {
  recipient: string;
  isAnswered: boolean;
}

const VideoCallText = (props: Props) => {
  if (props.isAnswered) {
    return (
      <div style={styles.videoCallTextViewAnswered}>
        <span style={styles.videoCallText}>
          Talking to patient: {props.recipient}
        </span>
      </div>
    );
  } else {
    return (
      <div style={styles.videoCallTextView}>
        <h1 style={styles.videoCallText}>Calling patient:</h1>
        <h1 style={styles.videoCallText}>{props.recipient}</h1>
      </div>
    );
  }
};

export default VideoCallText;
