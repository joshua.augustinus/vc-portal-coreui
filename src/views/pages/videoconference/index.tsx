import React from "react";
import { useSelector } from "react-redux";

import OpenTokContainer from "./OpenTokContainer";

import { RootState } from "../../../types";

const VideoConference = (props: any) => {
  const session = useSelector((state: RootState) => state.videoConference);
  const waitingRoom = useSelector((state: RootState) => state.waitingRoom);
  console.log("waitingRoom", waitingRoom);

  if (session.token) {
    return <OpenTokContainer {...session} />;
  } else return <></>;
};

export default VideoConference;
