import { ReactStyleSheet } from "../../../types";

const style: ReactStyleSheet = {
  subscriber: {
    position: "absolute",
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    zIndex: 10,
  },
  publisher: {
    position: "absolute",
    width: "360px",
    height: "240px",
    bottom: "10px",
    left: "10px",
    zIndex: 100,
    border: "3px solid white",
    borderRadius: "3px",
  },
  contentContainer: {
    height: "calc(100vh - 220px)",
    backgroundColor: "blue",
  },
  endCallButton: {
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    left: 0,
    right: 0,
    bottom: "10px",
    zIndex: 200,
    borderRadius: 50,
    height: 50,
    width: 50,
    color: "white",
  },
  videoCallTextView: {
    position: "absolute",
    marginLeft: "auto",
    marginRight: "auto",
    left: 0,
    right: 0,
    top: "35%",
    color: "white",
    zIndex: 200,
    textAlign: "center",
  },
  videoCallTextViewAnswered: {
    position: "absolute",
    right: 50,
    top: 0,
    color: "white",
    zIndex: 200,
    textAlign: "right",
  },
  videoCallText: {
    fontWeight: 200,
  },
};

export default style;
