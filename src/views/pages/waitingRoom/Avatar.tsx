import React from "react";

interface Props {
  ticketNumber: number;
}

const Avatar = (props: Props) => {
  const getAvatar = () => {
    let remainder = props.ticketNumber % 15; //will be 0-14

    return "avatars/" + remainder + ".png";
  };

  return (
    <div className="c-avatar">
      <img
        src={getAvatar()}
        className="c-avatar-img"
        alt="admin@bootstrapmaster.com"
      />
      <span className="c-avatar-status bg-success"></span>
    </div>
  );
};

export default Avatar;
