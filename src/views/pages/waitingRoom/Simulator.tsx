import * as signalrApi from "../../../actions/signalrApi";
import React from "react";
import { CSelect } from "@coreui/react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../../types";
import useInterval from "@use-it/interval";
let random = require("random-name");

const roomSizes = {
  small: { lowerBound: 1, upperBound: 9 },
  medium: { lowerBound: 4, upperBound: 9 },
  large: { lowerBound: 8, upperBound: 12 },
};

interface Props {
  isEnabled: boolean;
}

const Simulator = (props: Props) => {
  const dispatch = useDispatch();
  const waitingRoom = useSelector((state: RootState) => state.waitingRoom);
  const simName = useSelector(
    (state: RootState) => state.waitingRoom.simulatorSetting
  );
  const username = useSelector((state: RootState) => state.login.username);
  const roomSize = useSelector(
    (state: RootState) => state.waitingRoom.roomSize
  );

  let delay: number;

  if (!props.isEnabled) delay = null;
  else if (simName === "slow") delay = 8000;
  else if (simName === "medium") delay = 5000;
  else if (simName === "heavy") delay = 2500;
  else delay = null;

  const joinWaitingRoom = () => {
    let name = random.first() + " " + random.last();
    signalrApi.joinWaitingRoom(name);
  };

  const leaveWaitingRoom = () => {
    let ticketNumber = -1;
    for (let i = 0; i < waitingRoom.patients.length; i++) {
      let patient = waitingRoom.patients[i];
      if (patient.requestedDoctor !== username) {
        ticketNumber = patient.ticketNumber;
        break;
      }
    }

    signalrApi.leaveWaitingRoom(ticketNumber);
  };

  useInterval(() => {
    console.log("Inside setInterval");
    if (waitingRoom.patients.length <= roomSizes[roomSize].lowerBound) {
      joinWaitingRoom();
    } else if (waitingRoom.patients.length >= roomSizes[roomSize].upperBound) {
      leaveWaitingRoom();
    } else {
      let randomBoolean = Math.random() >= 0.5;
      if (randomBoolean) {
        leaveWaitingRoom();
      } else {
        joinWaitingRoom();
      }
    }
  }, delay);

  const trafficOptionHandler = (e: any) => {
    dispatch({
      type: "SIMULATOR_SETTING_UPDATED",
      simulatorSetting: e.target.value,
    });
  };

  const sizeHandler = (e: any) => {
    dispatch({
      type: "ROOM_SIZE_UPDATED",
      roomSize: e.target.value,
    });
  };

  return (
    <>
      <small className="text-muted mr-2">Simulate:</small>
      <CSelect
        onChange={trafficOptionHandler}
        value={simName}
        size="sm"
        name="select"
        id="select"
        style={{ width: 150 }}
      >
        <option value="slow">Slow Traffic</option>
        <option value="medium">Medium Traffic</option>
        <option value="heavy">Heavy Traffic</option>
        <option value="pause">Pause</option>
      </CSelect>
      <small className="text-muted mr-2 ml-2">Room size:</small>
      <CSelect
        onChange={sizeHandler}
        value={roomSize}
        size="sm"
        name="select"
        id="select"
        style={{ width: 150 }}
      >
        <option value="small">Small</option>
        <option value="medium">Medium</option>
        <option value="large">Large</option>
      </CSelect>
    </>
  );
};

export default Simulator;
