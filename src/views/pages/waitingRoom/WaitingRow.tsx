import React from "react";
import { CCol, CRow, CListGroupItem, CBadge } from "@coreui/react";
import { Patient } from "../../../types";
import BusyButton from "../../components/busyButton";
import Avatar from "./Avatar";
import "./styles.css";

const Fade = require("react-reveal/Fade");

const isDisabled = (patient: Patient, doctorUsername: string): boolean => {
  if (patient.requestedDoctor) {
    if (patient.requestedDoctor !== doctorUsername) {
      return true;
    }
  }

  return false;
};

interface Props {
  patient: Patient;
  doctorUsername: string;
  videoConferenceClickedHandler: (
    ticketNumber: number,
    name: string,
    registrationToken: string
  ) => void;
}

const WaitingRow = React.memo((props: Props) => {
  const patient = props.patient;
  const videoDisabled = isDisabled(patient, props.doctorUsername);
  const className = videoDisabled
    ? "text-primary"
    : "text-primary font-weight-bold";
  const rightText = patient.requestedDoctor ? (
    <>
      <small className="text-muted">Requested doctor:</small>
      <small className={className}>{patient.requestedDoctor}</small>
    </>
  ) : (
    ""
  );
  return (
    <CListGroupItem style={{ backgroundColor: "white" }}>
      <CRow>
        <CCol md="1">
          <Avatar ticketNumber={patient.ticketNumber} />
        </CCol>
        <CCol md="2">{patient.name}</CCol>
        <CCol md="3" className="d-flex">
          <CBadge color="success" className="mr-2">
            <span style={{ color: "yellow" }}>Medicare</span>
          </CBadge>
          <span className="text-muted">{patient.medicareNumber}</span>
        </CCol>
        <CCol md="3">
          <BusyButton
            disabled={videoDisabled}
            color="primary"
            onClick={() =>
              props.videoConferenceClickedHandler(
                patient.ticketNumber,
                patient.name,
                patient.registrationToken
              )
            }
          >
            Video Conference
          </BusyButton>
        </CCol>
        <CCol md="3" style={{ display: "flex", flexDirection: "column" }}>
          {rightText}
        </CCol>
      </CRow>
    </CListGroupItem>
  );
});

export { WaitingRow };
