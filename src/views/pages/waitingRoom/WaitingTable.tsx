import React, { useState, useEffect } from "react";
import { CListGroup } from "@coreui/react";
import { useDispatch, useSelector } from "react-redux";
import * as signalrApi from "../../../actions/signalrApi";
import { RootState } from "../../../types";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import TransitionGroup from "react-transition-group/TransitionGroup";

import "./styles.css";
import { WaitingRow } from "./WaitingRow";
import { CSSTransition } from "react-transition-group";

const Fade = require("react-reveal/Fade");

interface Props {
  videoConferenceClickedHandler: (
    ticketNumber: number,
    patientName: string,
    registrationToken: string
  ) => void;
}

const WaitingTable = (props: Props) => {
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();

  const username = useSelector((state: RootState) => state.login.username);
  const waitingRoom = useSelector((state: RootState) => state.waitingRoom);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    //fetch patients in waiting room
    dispatch(signalrApi.getWaitingRoom()).then(() => {
      setIsLoading(false);
    });
  }, []);

  let items = waitingRoom.patients.map((patient, index) => {
    return (
      <CSSTransition
        timeout={1000}
        classNames="item"
        key={patient.medicareNumber}
      >
        <WaitingRow
          patient={patient}
          doctorUsername={username}
          videoConferenceClickedHandler={props.videoConferenceClickedHandler}
        />
      </CSSTransition>
    );
  });

  if (isLoading) {
    let intViewportHeight = window.innerHeight - 300;
    return (
      <div
        className="d-flex justify-content-center"
        style={{ height: intViewportHeight, alignItems: "center" }}
      >
        Loading...
      </div>
    );
  } else if (waitingRoom.patients.length == 0)
    return (
      <div className="d-flex justify-content-center">
        There are no patients in the waiting room
      </div>
    );
  else {
    return (
      <Fade right>
        <CListGroup>
          <TransitionGroup>{items}</TransitionGroup>
        </CListGroup>
      </Fade>
    );
  }
};

export default WaitingTable;
