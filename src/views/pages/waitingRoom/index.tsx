import React, { useState, useEffect } from "react";
import { CCol, CRow, CCardBody } from "@coreui/react";
import WaitingTable from "./WaitingTable";
import Simulator from "./Simulator";
import { createSession } from "../../../actions/fcmApi";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { RootState } from "../../../types";
import { AnyAction } from "redux";
import { CardHeader, Card } from "../../components/card";

const WaitingRoom = (props: any) => {
  const history = useHistory();
  const [enableSim, setEnableSim] = useState(true);
  const dispatch = useDispatch<ThunkDispatch<RootState, void, AnyAction>>();

  useEffect(() => {
    setEnableSim(true);
    return () => {
      setEnableSim(false);
    };
  }, []);

  const vcClickedHandler = (
    ticketNumber: number,
    patientName: string,
    registrationToken: string
  ) => {
    setEnableSim(false);
    dispatch(createSession(ticketNumber, patientName, registrationToken)).then(
      (result: boolean) => {
        if (result) {
          history.push(
            "/videoconference/" +
              ticketNumber +
              "/" +
              encodeURIComponent(registrationToken)
          );
        }
      }
    );
  };

  return (
    <Card>
      <CardHeader>
        <CRow>
          <CCol md="4">Waiting Room </CCol>
          <CCol
            md="8"
            className="d-flex justify-content-end align-items-center"
          >
            <Simulator isEnabled={enableSim} />
          </CCol>
        </CRow>
      </CardHeader>
      <CCardBody>
        <WaitingTable videoConferenceClickedHandler={vcClickedHandler} />
      </CCardBody>
    </Card>
  );
};

export default WaitingRoom;
